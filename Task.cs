﻿using System;

public class Task
{
	public Task()
	{
	}

    private int mainTaskId;
    private string mainTaskTxt;

    public int MainTaskId { get => mainTaskId; set => mainTaskId = value; }
    public string MainTaskTxt { get => mainTaskTxt; set => mainTaskTxt = value; }
}
