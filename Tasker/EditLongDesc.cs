﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tasker
{
    public partial class EditLongDesc : Form
    {
        int idToEdit;
        string textToEdit;
        TaskerMain tskMainForm;

        public EditLongDesc()
        {
            InitializeComponent();
        }

        public EditLongDesc(string txt, int id, TaskerMain tskMain)
        {
            InitializeComponent();
            
            textToEdit = txt;
            idToEdit = id;
            tskMainForm = tskMain;
            tskMainForm.Enabled = false;
        }

        

        private void EditShortDesc_Load(object sender, EventArgs e)
        {
            taskEditTxt.Text = textToEdit;
            taskEditTxt.DeselectAll();
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            CloseForm();
        }

        private void editBtn_Click(object sender, EventArgs e)
        {
            TaskDbOperations tdb = new TaskDbOperations();
            tdb.UpdateLongDesc(taskEditTxt.Text, idToEdit);
            CloseForm();

        }


        public void CloseForm()
        {

            EditLongDesc thisForm = this;
            tskMainForm.Enabled = true;
            tskMainForm.ShowTasks();

            thisForm.Close();




        }

    }
}
