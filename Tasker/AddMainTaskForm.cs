﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tasker
{
    public partial class AddMainTaskForm : Form
    {

        TaskerMain tskMainForm;

        public AddMainTaskForm(TaskerMain tskForm)
        {
            InitializeComponent();
            tskMainForm = tskForm;
            tskMainForm.Enabled = false;

        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            CloseForm();
        }

        private void addTask_Click(object sender, EventArgs e)
        {
            TaskDbOperations tdb = new TaskDbOperations();
            tdb.AddTask(taskShortText.Text,taskLongText.Text);
            CloseForm();
        }




     

   


    public void CloseForm()
    {

        AddMainTaskForm thisForm = this;
        tskMainForm.Enabled = true;
        tskMainForm.ShowTasks();

        thisForm.Close();




    }


}
}
