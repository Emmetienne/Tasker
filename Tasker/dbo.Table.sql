﻿CREATE TABLE [dbo].[TaskID]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Description] NCHAR(180) NULL, 
    [Completed] BIT NULL
)
