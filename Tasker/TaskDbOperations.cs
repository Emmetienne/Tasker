﻿using System;
using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Windows.Forms;

public class TaskDbOperations
{
    SqlConnection connection;
    string connectionString;
    int rowCount;

    public int RowCount1 { get => rowCount; set => rowCount = value; }

    public TaskDbOperations()
	{

	}

    public void ConnectToDb()
    {






    }

    public Task ShowMainTask(int mainTaskId)
    {
        //set connection string
        connectionString = ConfigurationManager.ConnectionStrings["Tasker.Properties.Settings.TaskerDbConnectionString"].ConnectionString;
        connection = new SqlConnection(connectionString);
        //set query
        string selectSql = "select Id, Description, Completed, LongDescription from TaskId where Id = " + mainTaskId;

        //send sql command to the database
        SqlCommand com = new SqlCommand(selectSql, connection);

        Task tsk = new Task();

        try
        {
            connection.Open();

            using (SqlDataReader dbReader = com.ExecuteReader())
            {
                while (dbReader.Read())
                {
                    
                    tsk.MainTaskId = (int) (dbReader["Id"]);
                    Console.WriteLine("L'ID della task è " + tsk.MainTaskId);
                    tsk.MainTaskTxt = (string) (dbReader["Description"].ToString());
                    Console.WriteLine("La descrizione della task è :" + tsk.MainTaskTxt);
                    tsk.MainLongTaskTxt = (string)(dbReader["LongDescription"].ToString());
                    if (dbReader["Completed"].ToString().Equals("True"))
                    {
                        tsk.MainTaskCompleted = true;
                        
                    }
                    else
                    {
                        tsk.MainTaskCompleted = false;
                    
                    }

                    Console.WriteLine("Completed = " + tsk.MainTaskCompleted);
                }
            }
        }
        finally
        {
            connection.Close();
        }


        return tsk;
    }


    public Task ShowCompTask(int compTaskId)
    {
        //set connection string
        connectionString = ConfigurationManager.ConnectionStrings["Tasker.Properties.Settings.TaskerDbConnectionString"].ConnectionString;
        connection = new SqlConnection(connectionString);
        //set query
        string selectSql = "select Id, Description, Completed, LongDescription from TaskId where Id = " + compTaskId;

        //send sql command to the database
        SqlCommand com = new SqlCommand(selectSql, connection);

        Task tsk = new Task();

        try
        {
            connection.Open();

            using (SqlDataReader dbReader = com.ExecuteReader())
            {
                while (dbReader.Read())
                {

                    tsk.MainTaskId = (int)(dbReader["Id"]);
                    Console.WriteLine("L'ID della task è " + tsk.MainTaskId);
                    tsk.MainTaskTxt = (string)(dbReader["Description"].ToString());
                    Console.WriteLine("La descrizione della task è :" + tsk.MainTaskTxt);
                    tsk.MainLongTaskTxt = (string)(dbReader["LongDescription"].ToString());
                    if (dbReader["Completed"].ToString().Equals("True"))
                    {
                        tsk.MainTaskCompleted = true;

                    }
                    else
                    {
                        tsk.MainTaskCompleted = false;

                    }

                    Console.WriteLine("Completed = " + tsk.MainTaskCompleted);
                }
            }
        }
        finally
        {
            connection.Close();
        }


        return tsk;
    }


    public void AddTask(string shortTxt, string longTxt)
    {
        //set connection string
        connectionString = ConfigurationManager.ConnectionStrings["Tasker.Properties.Settings.TaskerDbConnectionString"].ConnectionString;
        connection = new SqlConnection(connectionString);

        connection.Open();


        using (SqlCommand com = new SqlCommand("INSERT TaskId VALUES (@a1, @a2, @a3)", connection)) 
        {

            com.Parameters.AddWithValue("@a1", shortTxt);
            com.Parameters.AddWithValue("@a2", 0);
            com.Parameters.AddWithValue("@a3", longTxt);
            com.ExecuteScalar();
            connection.Close();
        }





    }

    public void UpdateShortDesc(string txt, int id)
    {
        //set connection string
        connectionString = ConfigurationManager.ConnectionStrings["Tasker.Properties.Settings.TaskerDbConnectionString"].ConnectionString;
        connection = new SqlConnection(connectionString);
      
        connection.Open();

        using (SqlCommand com = new SqlCommand("UPDATE TaskId SET Description = @a1 WHERE Id = @a2", connection))
        {
            
            com.Parameters.AddWithValue("@a1", txt);
            com.Parameters.AddWithValue("@a2",(int) id);
            com.ExecuteScalar();        
            connection.Close();
        }




    }

    public void UpdateLongDesc(string txt, int id)
    {
        //set connection string
        connectionString = ConfigurationManager.ConnectionStrings["Tasker.Properties.Settings.TaskerDbConnectionString"].ConnectionString;
        connection = new SqlConnection(connectionString);
        connection.Open();

        using (SqlCommand com = new SqlCommand("UPDATE TaskId SET LongDescription = @a1 WHERE Id = @a2", connection))
        {

            com.Parameters.AddWithValue("@a1", txt);
            com.Parameters.AddWithValue("@a2", (int)id);
            com.ExecuteScalar();
            connection.Close();
        }
        




    }

    public void DeleteTask(int id)
    {

        DialogResult dialogResult = MessageBox.Show("Are you sure to delete this task?", "Delete", MessageBoxButtons.YesNo);
        if (dialogResult == DialogResult.Yes)
        {
            //set connection string
            connectionString = ConfigurationManager.ConnectionStrings["Tasker.Properties.Settings.TaskerDbConnectionString"].ConnectionString;
            connection = new SqlConnection(connectionString);
            connection.Open();

            using (SqlCommand com = new SqlCommand("DELETE TaskId WHERE ID = @a1", connection))
            {

                com.Parameters.AddWithValue("@a1", id);

                com.ExecuteScalar();
                connection.Close();
            }



        }
      

    }

    public bool SetCompleted(bool comp, int id)
    {

       
            //set connection string
            connectionString = ConfigurationManager.ConnectionStrings["Tasker.Properties.Settings.TaskerDbConnectionString"].ConnectionString;
            connection = new SqlConnection(connectionString);
            connection.Open();

            using (SqlCommand com = new SqlCommand("UPDATE TaskId SET Completed = @a1 WHERE Id = @a2", connection))
            {

                if (comp == true)
                {
                    com.Parameters.AddWithValue("@a1", 1);
                com.Parameters.AddWithValue("@a2", id);
                com.ExecuteScalar();
                connection.Close();


                Console.WriteLine("TASK " + id + "SET AS COMPLETED");
                    return true;

                }
                else
                {
                    com.Parameters.AddWithValue("@a1", 0);
                com.Parameters.AddWithValue("@a2", id);
                com.ExecuteScalar();
                connection.Close();



                Console.WriteLine("TASK " + id + "SET AS NOT COMPLETED");
                    return false;
                }


                
            }



      
        




    }


    public bool CheckIfNotEmpty()
    {

        //int rowsCount;
        //set connection string
        connectionString = ConfigurationManager.ConnectionStrings["Tasker.Properties.Settings.TaskerDbConnectionString"].ConnectionString;
        connection = new SqlConnection(connectionString);
       // connection.Open();

        using (SqlCommand com = new SqlCommand("SELECT COUNT(Id) FROM TaskId WHERE Completed = 0",connection))
        {

            connection.Open();

            rowCount = (int) com.ExecuteScalar();
            Console.WriteLine("NUMERO RIGHE: " + rowCount);
            connection.Close();
        }

        if ( rowCount > 0)
        {

            return true;

        }
        else
        {

            return false;

        }
        
        

           


    }


    public bool CheckIfNotEmptyCompleted()
    {

        //int rowsCount;
        //set connection string
        connectionString = ConfigurationManager.ConnectionStrings["Tasker.Properties.Settings.TaskerDbConnectionString"].ConnectionString;
        connection = new SqlConnection(connectionString);
        // connection.Open();

        using (SqlCommand com = new SqlCommand("SELECT COUNT(Id) FROM TaskId WHERE Completed = 1", connection))
        {

            connection.Open();

            rowCount = (int)com.ExecuteScalar();
            Console.WriteLine("NUMERO RIGHE: " + rowCount);
            connection.Close();
        }

        if (rowCount > 0)
        {

            return true;

        }
        else
        {

            return false;

        }






    }
}



