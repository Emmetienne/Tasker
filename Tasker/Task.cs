﻿using System;

public class Task
{
	public Task()
	{
	}

    private int mainTaskId;
    private string mainTaskTxt;
    private string mainLongTaskTxt;
    private bool mainTaskCompleted;

    public int MainTaskId { get => mainTaskId; set => mainTaskId = value; }
    public string MainTaskTxt { get => mainTaskTxt; set => mainTaskTxt = value; }
    public bool MainTaskCompleted { get => mainTaskCompleted; set => mainTaskCompleted = value; }
    public string MainLongTaskTxt { get => mainLongTaskTxt; set => mainLongTaskTxt = value; }
}
