﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tasker
{
    public partial class EditShortDesc : Form
    {
        int idToEdit;
        string textToEdit;
        TaskerMain tskMainForm;

        public EditShortDesc()
        {
            InitializeComponent();
        }

        public EditShortDesc(string txt, int id, TaskerMain tskMain)
        {
            InitializeComponent();
            
            textToEdit = txt;
            idToEdit = id;
            tskMainForm = tskMain;
            tskMainForm.Enabled = false;
        }

        

        private void EditShortDesc_Load(object sender, EventArgs e)
        {
            taskEditTxt.Text = textToEdit;
            taskEditTxt.DeselectAll();
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            CloseForm();
        }

        private void editBtn_Click(object sender, EventArgs e)
        {
            TaskDbOperations tdb = new TaskDbOperations();
            tdb.UpdateShortDesc(taskEditTxt.Text, idToEdit);
            CloseForm();

        }

        private void EditShortDesc_FormClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            tskMainForm.Enabled = true;
            tskMainForm.ShowTasks();

            tskMainForm.Show();
        }

        public void CloseForm()
        {

            EditShortDesc thisForm = this;
            tskMainForm.Enabled = true;
            tskMainForm.ShowTasks();

            thisForm.Close();






        }

    }
}
