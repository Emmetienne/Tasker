﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace Tasker
{
    public partial class TaskerMain : Form
    {
        SqlConnection connection;
        string connectionString;
        TaskDbOperations tdb; 

        static int currentId;
        static int currentCompId;


        public TaskerMain()
        {
            InitializeComponent();

            connectionString = ConfigurationManager.ConnectionStrings["Tasker.Properties.Settings.TaskerDbConnectionString"].ConnectionString;
           
           


        }

        private void TaskerMain_Load(object sender, EventArgs e)
        {
            tdb = new TaskDbOperations();
            ShowTasks();
            ShowCompletedTasks();


        }


        public void ShowTasks()
        {

            
            
            if (tdb.CheckIfNotEmpty() == true)
            {
                splitContainer1.Panel2.Enabled = true;

                using (connection = new SqlConnection(connectionString))
                using (SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM TaskId WHERE Completed = 0", connection))
                {



                    DataTable taskList = new DataTable();


                    adapter.Fill(taskList);

                    taskView.DisplayMember = "Description";
                    taskView.ValueMember = "Id";

                    taskView.DataSource = taskList;

                    
                }


            }
            else
            {
                splitContainer1.Panel2.Enabled = false;
                
                taskView.DataSource = null;
                mainTaskLabel.Text = "";
                longDescriptionTask.Text = "";
                completedCheck.Checked = false;

            }






        }

        public void ShowCompletedTasks()
        {



            if (tdb.CheckIfNotEmptyCompleted() == true)
            {
                completedContainer.Panel2.Enabled = true;
                

                using (connection = new SqlConnection(connectionString))
                using (SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM TaskId WHERE Completed = 1", connection))
                {



                    DataTable taskList = new DataTable();


                    adapter.Fill(taskList);

                    completedList.DisplayMember = "Description";
                    completedList.ValueMember = "Id";

                    completedList.DataSource = taskList;


                }


            }
            else
            {
                completedContainer.Panel2.Enabled = false;
                completedList.DataSource = null;
                cmpLblShort.Text = "";
                cmpLngLbl.Text = "";


            }






        }




        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var form = new AddMainTaskForm(this);
            form.Show();
        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void taskView_SelectedIndexChanged(object sender, EventArgs e)
        {
            // MessageBox.Show(taskView.SelectedValue.ToString());
           if (tdb.CheckIfNotEmpty() == true)
            {

                currentId = (int)taskView.SelectedValue;


            }






            Task t = new Task();
            t = tdb.ShowMainTask(currentId);

            mainTaskLabel.Text = t.MainTaskTxt;
            longDescriptionTask.Text = t.MainLongTaskTxt;
            completedCheck.Checked = t.MainTaskCompleted;

            
           

        }

        private void editShortDescription_Click(object sender, EventArgs e)
        {
            var form = new EditShortDesc(mainTaskLabel.Text, currentId, this);
            form.Show();
            
        }

        private void editLongDescription_Click(object sender, EventArgs e)
        {
            var form = new EditLongDesc(longDescriptionTask.Text, currentId, this);
            form.Show();
        }

        private void longDescriptionTask_Click(object sender, EventArgs e)
        {

        }

        private void deleteTaskBtn_Click(object sender, EventArgs e)
        {
           
            tdb.DeleteTask(currentId);
            ShowTasks();
            ShowCompletedTasks();
        }

        private void completedCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (completedCheck.Checked == true)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure to flag this task as completed?", "Complete Task", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    tdb.SetCompleted(true, currentId);

                }

                else
                {
                    completedCheck.Checked = false;


                }

                ShowTasks();
                ShowCompletedTasks();


            }
           


        }

        private void completedList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tdb.CheckIfNotEmptyCompleted() == true)
            {

               currentCompId = (int)completedList.SelectedValue;


            }






            Task tComp = new Task();
            tComp = tdb.ShowCompTask(currentCompId);

            cmpLblShort.Text = tComp.MainTaskTxt;
            cmpLngLbl.Text = tComp.MainLongTaskTxt;
            



        }


        private void tabControl1_Selected(Object sender, TabControlEventArgs e)
        {
            MessageBox.Show(tabControl1.SelectedIndex.ToString(), "Selected Event");
            Console.WriteLine("INDEX CHANGED");


        }

        private void dltCompTsk_Click_1(object sender, EventArgs e)
        {
            tdb.DeleteTask(currentCompId);
            ShowTasks();
            ShowCompletedTasks();
        }

        private void setNotCompleted_Click(object sender, EventArgs e)
        {

            DialogResult dialogResult = MessageBox.Show("Are you sure to flag this task as not completed?", "Set task as not completed", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                tdb.SetCompleted(false, currentCompId);

            }

            else
            {
                


            }

            ShowTasks();
            ShowCompletedTasks();
           
        }
    }
}
