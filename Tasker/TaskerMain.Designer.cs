﻿using System;

namespace Tasker
{
    partial class TaskerMain
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TaskerMain));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.taskView = new System.Windows.Forms.ListBox();
            this.deleteTaskBtn = new System.Windows.Forms.Button();
            this.completedCheck = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.editLongDescription = new System.Windows.Forms.Button();
            this.longDescriptionTask = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.editShortDescription = new System.Windows.Forms.Button();
            this.mainTaskLabel = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.completedContainer = new System.Windows.Forms.SplitContainer();
            this.completedList = new System.Windows.Forms.ListBox();
            this.dltCompTsk = new System.Windows.Forms.Button();
            this.setNotCompleted = new System.Windows.Forms.Button();
            this.lngCompleted = new System.Windows.Forms.GroupBox();
            this.cmpLngLbl = new System.Windows.Forms.Label();
            this.shrtCompleted = new System.Windows.Forms.GroupBox();
            this.cmpLblShort = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.completedContainer)).BeginInit();
            this.completedContainer.Panel1.SuspendLayout();
            this.completedContainer.Panel2.SuspendLayout();
            this.completedContainer.SuspendLayout();
            this.lngCompleted.SuspendLayout();
            this.shrtCompleted.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(650, 325);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.splitContainer1);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(642, 299);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Open";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(7, 36);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.taskView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.deleteTaskBtn);
            this.splitContainer1.Panel2.Controls.Add(this.completedCheck);
            this.splitContainer1.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel2.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Panel2_Paint);
            this.splitContainer1.Size = new System.Drawing.Size(629, 257);
            this.splitContainer1.SplitterDistance = 209;
            this.splitContainer1.TabIndex = 1;
            // 
            // taskView
            // 
            this.taskView.FormattingEnabled = true;
            this.taskView.Location = new System.Drawing.Point(4, 4);
            this.taskView.Name = "taskView";
            this.taskView.Size = new System.Drawing.Size(202, 251);
            this.taskView.TabIndex = 0;
            this.taskView.SelectedIndexChanged += new System.EventHandler(this.taskView_SelectedIndexChanged);
            // 
            // deleteTaskBtn
            // 
            this.deleteTaskBtn.Location = new System.Drawing.Point(313, 231);
            this.deleteTaskBtn.Name = "deleteTaskBtn";
            this.deleteTaskBtn.Size = new System.Drawing.Size(100, 23);
            this.deleteTaskBtn.TabIndex = 5;
            this.deleteTaskBtn.Text = "Delete this task";
            this.deleteTaskBtn.UseVisualStyleBackColor = true;
            this.deleteTaskBtn.Click += new System.EventHandler(this.deleteTaskBtn_Click);
            // 
            // completedCheck
            // 
            this.completedCheck.AutoSize = true;
            this.completedCheck.Location = new System.Drawing.Point(13, 209);
            this.completedCheck.Name = "completedCheck";
            this.completedCheck.Size = new System.Drawing.Size(76, 17);
            this.completedCheck.TabIndex = 4;
            this.completedCheck.Text = "Completed";
            this.completedCheck.UseVisualStyleBackColor = true;
            this.completedCheck.CheckedChanged += new System.EventHandler(this.completedCheck_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.editLongDescription);
            this.groupBox2.Controls.Add(this.longDescriptionTask);
            this.groupBox2.Location = new System.Drawing.Point(4, 68);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(409, 135);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Detailed description";
            // 
            // editLongDescription
            // 
            this.editLongDescription.Location = new System.Drawing.Point(355, 106);
            this.editLongDescription.Name = "editLongDescription";
            this.editLongDescription.Size = new System.Drawing.Size(48, 23);
            this.editLongDescription.TabIndex = 2;
            this.editLongDescription.Text = "... edit";
            this.editLongDescription.UseVisualStyleBackColor = true;
            this.editLongDescription.Click += new System.EventHandler(this.editLongDescription_Click);
            // 
            // longDescriptionTask
            // 
            this.longDescriptionTask.Location = new System.Drawing.Point(6, 16);
            this.longDescriptionTask.Name = "longDescriptionTask";
            this.longDescriptionTask.Size = new System.Drawing.Size(397, 116);
            this.longDescriptionTask.TabIndex = 1;
            this.longDescriptionTask.Click += new System.EventHandler(this.longDescriptionTask_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.editShortDescription);
            this.groupBox1.Controls.Add(this.mainTaskLabel);
            this.groupBox1.Location = new System.Drawing.Point(4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(409, 57);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Short description";
            // 
            // editShortDescription
            // 
            this.editShortDescription.Location = new System.Drawing.Point(355, 28);
            this.editShortDescription.Name = "editShortDescription";
            this.editShortDescription.Size = new System.Drawing.Size(48, 23);
            this.editShortDescription.TabIndex = 1;
            this.editShortDescription.Text = "... edit";
            this.editShortDescription.UseVisualStyleBackColor = true;
            this.editShortDescription.Click += new System.EventHandler(this.editShortDescription_Click);
            // 
            // mainTaskLabel
            // 
            this.mainTaskLabel.AutoEllipsis = true;
            this.mainTaskLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mainTaskLabel.Location = new System.Drawing.Point(6, 16);
            this.mainTaskLabel.Name = "mainTaskLabel";
            this.mainTaskLabel.Size = new System.Drawing.Size(397, 26);
            this.mainTaskLabel.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(630, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Add Task";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.completedContainer);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(642, 299);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Closed";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // completedContainer
            // 
            this.completedContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.completedContainer.Location = new System.Drawing.Point(3, 3);
            this.completedContainer.Name = "completedContainer";
            // 
            // completedContainer.Panel1
            // 
            this.completedContainer.Panel1.Controls.Add(this.completedList);
            // 
            // completedContainer.Panel2
            // 
            this.completedContainer.Panel2.Controls.Add(this.dltCompTsk);
            this.completedContainer.Panel2.Controls.Add(this.setNotCompleted);
            this.completedContainer.Panel2.Controls.Add(this.lngCompleted);
            this.completedContainer.Panel2.Controls.Add(this.shrtCompleted);
            this.completedContainer.Size = new System.Drawing.Size(636, 293);
            this.completedContainer.SplitterDistance = 212;
            this.completedContainer.TabIndex = 0;
            // 
            // completedList
            // 
            this.completedList.FormattingEnabled = true;
            this.completedList.Location = new System.Drawing.Point(3, 3);
            this.completedList.Name = "completedList";
            this.completedList.Size = new System.Drawing.Size(206, 290);
            this.completedList.TabIndex = 0;
            this.completedList.SelectedIndexChanged += new System.EventHandler(this.completedList_SelectedIndexChanged);
            // 
            // dltCompTsk
            // 
            this.dltCompTsk.Location = new System.Drawing.Point(222, 267);
            this.dltCompTsk.Name = "dltCompTsk";
            this.dltCompTsk.Size = new System.Drawing.Size(195, 23);
            this.dltCompTsk.TabIndex = 3;
            this.dltCompTsk.Text = "Delete task";
            this.dltCompTsk.UseVisualStyleBackColor = true;
            this.dltCompTsk.Click += new System.EventHandler(this.dltCompTsk_Click_1);
            // 
            // setNotCompleted
            // 
            this.setNotCompleted.Location = new System.Drawing.Point(4, 267);
            this.setNotCompleted.Name = "setNotCompleted";
            this.setNotCompleted.Size = new System.Drawing.Size(212, 23);
            this.setNotCompleted.TabIndex = 2;
            this.setNotCompleted.Text = "Set as not completed";
            this.setNotCompleted.UseVisualStyleBackColor = true;
            this.setNotCompleted.Click += new System.EventHandler(this.setNotCompleted_Click);
            // 
            // lngCompleted
            // 
            this.lngCompleted.Controls.Add(this.cmpLngLbl);
            this.lngCompleted.Location = new System.Drawing.Point(4, 110);
            this.lngCompleted.Name = "lngCompleted";
            this.lngCompleted.Size = new System.Drawing.Size(413, 151);
            this.lngCompleted.TabIndex = 1;
            this.lngCompleted.TabStop = false;
            this.lngCompleted.Text = "Detailed description";
            // 
            // cmpLngLbl
            // 
            this.cmpLngLbl.AutoEllipsis = true;
            this.cmpLngLbl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmpLngLbl.Location = new System.Drawing.Point(6, 16);
            this.cmpLngLbl.Name = "cmpLngLbl";
            this.cmpLngLbl.Size = new System.Drawing.Size(401, 132);
            this.cmpLngLbl.TabIndex = 2;
            // 
            // shrtCompleted
            // 
            this.shrtCompleted.Controls.Add(this.cmpLblShort);
            this.shrtCompleted.Location = new System.Drawing.Point(4, 4);
            this.shrtCompleted.Name = "shrtCompleted";
            this.shrtCompleted.Size = new System.Drawing.Size(413, 100);
            this.shrtCompleted.TabIndex = 0;
            this.shrtCompleted.TabStop = false;
            this.shrtCompleted.Text = "Short description";
            // 
            // cmpLblShort
            // 
            this.cmpLblShort.AutoEllipsis = true;
            this.cmpLblShort.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmpLblShort.Location = new System.Drawing.Point(6, 16);
            this.cmpLblShort.Name = "cmpLblShort";
            this.cmpLblShort.Size = new System.Drawing.Size(401, 81);
            this.cmpLblShort.TabIndex = 1;
            // 
            // TaskerMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(674, 343);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(690, 39);
            this.Name = "TaskerMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tasker";
            this.Load += new System.EventHandler(this.TaskerMain_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.completedContainer.Panel1.ResumeLayout(false);
            this.completedContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.completedContainer)).EndInit();
            this.completedContainer.ResumeLayout(false);
            this.lngCompleted.ResumeLayout(false);
            this.shrtCompleted.ResumeLayout(false);
            this.ResumeLayout(false);

        }

      

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListBox taskView;
        private System.Windows.Forms.Label mainTaskLabel;
        private System.Windows.Forms.Label longDescriptionTask;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button editShortDescription;
        private System.Windows.Forms.Button editLongDescription;
        private System.Windows.Forms.CheckBox completedCheck;
        private System.Windows.Forms.Button deleteTaskBtn;
        private System.Windows.Forms.SplitContainer completedContainer;
        private System.Windows.Forms.ListBox completedList;
        private System.Windows.Forms.Button setNotCompleted;
        private System.Windows.Forms.GroupBox lngCompleted;
        private System.Windows.Forms.GroupBox shrtCompleted;
        private System.Windows.Forms.Label cmpLngLbl;
        private System.Windows.Forms.Label cmpLblShort;
        private System.Windows.Forms.Button dltCompTsk;
        private EventHandler dltCompTsk_Click;
    }
}

